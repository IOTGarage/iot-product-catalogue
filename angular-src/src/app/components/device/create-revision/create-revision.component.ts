import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { DevicesService } from "../../../services/devices/devices.service";
import { FeaturesService } from "../../../services/devices/features.service";
import { UploadFileService } from "../../../services/upload-file/upload-file.service";

import { Feature } from "../../../models/feature";
import { Device } from "../../../models/device";
import { Revision } from "../../../models/revision";

import { FlashMessagesService } from "angular2-flash-messages";
import { NgForm } from "@angular/forms";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import {EvidenceViewerModalComponent} from "../../evidence-viewer-modal/evidence-viewer-modal.component";


@Component({
  selector: 'app-create-revision',
  templateUrl: './create-revision.component.html',
  styleUrls: ['./create-revision.component.scss']
})
export class CreateRevisionComponent implements OnInit {
// to populate and store device details
  device: Device = new Device();
  revision: Revision = new Revision();
  feature: Feature = new Feature();
  oldImage: String;

  instructions: String;

  featureButton: String = 'Add';

  // stores db features
  predefinedFeatures = [];
  // stores already chosen features for device
  chosenFeatures = [];
  featuresAdded: boolean = false;
  selectedFeature = '';

  selectedMedia = 'ph';
  evidenceIsImage: boolean = false;
  evidenceIsVideo: boolean = false;
  evidenceIsPdf: boolean = false;
  evidenceIsAudio: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private deviceService: DevicesService,
    private featuresService: FeaturesService,
    private uploadService: UploadFileService,
    private flashMessage: FlashMessagesService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.loadDevice();
    this.loadFeaturesFromDb();
  }

  openModal() {
    const modalRef = this.modalService.open(EvidenceViewerModalComponent, {size: 'lg'});
    modalRef.componentInstance.title = this.feature.title;
    modalRef.componentInstance.src = this.feature.src;
    modalRef.componentInstance.type = this.feature.evidenceType;
    modalRef.componentInstance.shortcut = this.feature.shortcut;
  }

  loadDevice () {
    this.route.params.subscribe(params =>
      {
        this.device._id = params.id;
        const userType = JSON.parse(localStorage.getItem('user')).userType;

        if (userType === 'admin' || userType === 'student') {
          this.router.navigate([`/view-device/${params.id}`]);
        }
      },
      err => {
        console.log(err);
        return false;
      });

    this.deviceService.getDeviceById(this.device._id).subscribe(response => {
        if (response.success) {
          this.device.image = response.device.image;
          this.oldImage = response.device.image;

          this.device.name = response.device.name;
          this.device.manufacturer = response.device.manufacturer;

          this.instructions = 'You cannot view any features';
        }

        else {
          console.log(response.device.msg);
          return false;
        }


      },
      err => {
        console.log(err);
        return false;
      });
  }

  loadFeaturesFromDb () {
    this.featuresService.getFeatures().subscribe(response => {
        if (response.success) {
          response.features.forEach((element) => {
            this.predefinedFeatures.push({
              category: element.category,
              listOfFeatures: element.listOfFeatures
            });
          });
        }

        else {
          console.log('No predefined features');
        }},
      err => {
        console.log(err);
        return false;
      });
  }

  // initialise media selection
  falsifyAllMedia () {
    this.evidenceIsImage = false;
    this.evidenceIsVideo = false;
    this.evidenceIsPdf = false;
    this.evidenceIsAudio = false;
  }

  checkEvidenceType() {
    if (this.selectedMedia === 'image') {
      this.evidenceIsImage = true;
    }

    else if (this.selectedMedia === 'video') {
      this.evidenceIsVideo = true;
    }

    else if (this.selectedMedia === 'pdf') {
      this.evidenceIsPdf = true;
    }

    else if (this.selectedMedia === 'audio') {
      this.evidenceIsAudio = true;
    }
  }

  // handles media selection for upload
  onMediaSelect () {
    this.resetEvidence();
    this.falsifyAllMedia();
    this.checkEvidenceType();
  }

  // populates addfeature section with data from feature section so user can edit
  populateAddFeatureSection (createdFeature) {
    this.feature.title = createdFeature.evidence.title;
    this.feature.origin = createdFeature.evidence.origin;
    this.feature.purpose = createdFeature.evidence.purpose;
    this.feature.src = createdFeature.evidence.src;
    this.feature.thumbnail = createdFeature.evidence.thumbnail;
    this.feature.evidenceType = createdFeature.evidence.type;
    this.feature.shortcut = createdFeature.evidence.shortcut;

    this.selectedFeature = createdFeature.feature;
    this.selectedMedia = createdFeature.evidence.type;

    this.falsifyAllMedia();
    this.checkEvidenceType();
  }

  onFeatureClick (feature) {
    this.featureButton = 'Update';
    this.populateAddFeatureSection(feature);
  }

  showUploadButton() {
    this.feature.src = null;
    this.feature.thumbnail = null;
    this.feature.evidenceType = null;
    this.feature.shortcut = 0;
  }

  // find category from feature
  getCategory() {
    const feature = this.selectedFeature;
    const featuresList = this.predefinedFeatures;

    for (let i = 0; i < this.predefinedFeatures.length; i++) {
      if (featuresList[i].listOfFeatures.indexOf(feature) !== -1) {
        return featuresList[i].category;
      }
    }
  }

  // create base feature object on form submission
  createFeatureObject () {
    return {
      category: this.getCategory(),
      feature: this.selectedFeature,
      title: this.feature.title,
      origin: this.feature.origin,
      purpose: this.feature.purpose,
      src: this.feature.src,
      thumbnail: this.feature.thumbnail,
      type: this.feature.evidenceType,
      shortcut: this.feature.shortcut
    };
  }

  // create a new feature object for chosenFeatures
  static createNewFeature (feature) {
    return {
      feature: feature.feature,
      evidence: {
        title: feature.title,
        origin: feature.origin,
        purpose: feature.purpose,
        src: feature.src,
        thumbnail: feature.thumbnail,
        type: feature.type,
        shortcut: feature.shortcut
      }
    }
  }

  static featureExists (listOfFeatures, feature) {
    let found = false;

    for (let i = 0; i < listOfFeatures.length; i++) {
      if (listOfFeatures[i].feature === feature) {
        found = true;
      }
    }
    return found;
  }

  updateFeature(i, updatedFeature) {
    for (let j = 0; j < this.chosenFeatures[i].features.length; j++) {
      if (this.chosenFeatures[i].features[j].feature === updatedFeature.feature) {
        this.chosenFeatures[i].features[j] = updatedFeature;
      }
    }
  }

  static createNewCategory (feature) {
    return {
      category: feature.category,
      features: [CreateRevisionComponent.createNewFeature(feature)]
    }
  }

  addFeatureToChosenFeatures (feature) {
    // if not first feature, add to existing object
    if (this.chosenFeatures.length > 0) {
      let found = false;

      for (let i = 0; i < this.chosenFeatures.length; i++) {
        // if category already exists, add feature to category object
        if (this.chosenFeatures[i].category === feature.category) {
          const newFeature = CreateRevisionComponent.createNewFeature(feature);
          found = true;
          // if feature does not already exist, add to chosenFeatures
          if (!CreateRevisionComponent.featureExists(this.chosenFeatures[i].features, feature.feature)){
            this.chosenFeatures[i].features.push(newFeature);
            this.flashMessage.show('Feature added', {cssClass: 'alert-success', timeout: 3000});
          }

          else {
            this.updateFeature(i, newFeature);
            this.flashMessage.show('Feature updated', {cssClass: 'alert-success', timeout: 3000});
          }
        }
      }

      // if category does not exist, create a new category object
      if (!found) {
        const newCategory = CreateRevisionComponent.createNewCategory(feature);
        this.chosenFeatures.push(newCategory);
      }
    }

    // if first feature, define chosenFeatures with added feature
    else {
      const newCategory = CreateRevisionComponent.createNewCategory(feature);
      this.chosenFeatures = [newCategory]
    }
  }

  resetEvidence() {
    this.feature.src = null;
    this.feature.thumbnail = null;
    this.feature.shortcut = 0;
    this.feature.evidenceType = null;
  }

  resetForm (form: NgForm) {
    this.falsifyAllMedia();
    // assign default select options
    form.reset({selectFeature: '', selectMedia: 'ph'});

    this.feature.title = null;
    this.feature.origin = null;
    this.feature.purpose = null;

    this.resetEvidence();

    this.featureButton = 'Add';
  }

  onAddFeatureClick (form) {
    const feature = this.createFeatureObject();

    this.addFeatureToChosenFeatures(feature);

    // remove 'no features' header
    if (this.featuresAdded === false) {
      this.featuresAdded = true
    }

    // initialise form and variables
    this.resetForm(form);
  }

  sendRevisionToDb (revision) {
    this.deviceService.createRevision(revision).subscribe(data => {
      if (data.success) {
        this.flashMessage.show(`New edition submitted, please contact an admin to merge.`, {cssClass: 'alert-success', timeout: 5000});
        this.router.navigate(['/']);
      }

      else {
        this.flashMessage.show('Something went wrong', {cssClass: 'alert-danger', timeout: 3000});
      }
    });
  }

  createRevision() {
    this.revision._id = this.device._id;

    if (this.device.price.charAt(0) !== '£') {
      this.device.price = `£${this.device.price}`
    }

    if (this.device.image !== this.oldImage) {
      this.revision.revision = {
        price: this.device.price,
        description: this.device.description,
        image: this.device.image,
        creator: JSON.parse(localStorage.getItem('user')).username,
        technology: this.chosenFeatures
      }
    }

    else {
      this.revision.revision = {
        price: this.device.price,
        description: this.device.description,
        creator: JSON.parse(localStorage.getItem('user')).username,
        technology: this.chosenFeatures
      }
    }
  }

  onSaveSubmit () {
    this.createRevision();

    this.deviceService.getDeviceById(this.device._id).subscribe(response => {
        if (response.success) {
          this.sendRevisionToDb(this.revision);
        }

        else {
          console.log(response.msg);
          return false;
        }

      },
      err => {
        console.log(err);
        return false;
      }
    );
  }

  handleImageDeviceUpload(res) {
    this.device.image = res.file;
    this.flashMessage.show(res.msg, {cssClass: 'alert-success', timeout: 3000});
  }

  handleImageEvidenceUpload(res) {
    this.feature.src = res.files.image;
    this.feature.thumbnail = res.files.thumbnail;
    this.feature.evidenceType = res.type;
  }

  handleVideoEvidenceUpload(res) {
    this.feature.src = res.files.file;
    this.feature.thumbnail = res.files.thumbnail;
    this.feature.shortcut = res.files.shortcut;
    this.feature.evidenceType = res.type;

  }

  handleUploadError(res) {
    console.log(res.msg);
    this.flashMessage.show('Something went wrong', {cssClass: 'alert-danger', timeout: 3000});
  }

  handleAudioVideoEvidenceUpload(res) {
    this.feature.src = res.file.file;
    this.feature.shortcut = res.file.shortcut;
    this.feature.evidenceType = res.type;
  }

  deleteMedia(thumbnail, src) {
    let files;

    if (thumbnail !== null) {
      files = {filesToDelete: [thumbnail, src]};
    }

    else {
      files = {filesToDelete: [src]};
    }

    this.uploadService.delete(files).subscribe(response => {
        if (!response.success) {
          console.log(response.msg);
        }},
      err => {
        console.log(err);
        return false;
      });
  }

  deleteFeature (categoryName, feature) {
    for (let i = 0; i < this.chosenFeatures.length; i++) {
      if (this.chosenFeatures[i].category === categoryName) {
        for (let j = 0; j < this.chosenFeatures[i].features.length; j++) {
          if (this.chosenFeatures[i].features[j].feature === feature.feature) {
            this.deleteMedia(feature.evidence.thumbnail, feature.evidence.src);
            this.chosenFeatures[i].features.splice(j, 1);

            if (this.chosenFeatures[i].features.length === 0) {
              this.chosenFeatures.splice(i, 1);

              if (this.chosenFeatures.length === 0) {
                this.featuresAdded = false;
              }
            }

            break;
          }
        }
        break;
      }
    }
  }
}
