import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvidenceViewerModalComponent } from './evidence-viewer-modal.component';

describe('EvidenceViewerModalComponent', () => {
  let component: EvidenceViewerModalComponent;
  let fixture: ComponentFixture<EvidenceViewerModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvidenceViewerModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvidenceViewerModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
