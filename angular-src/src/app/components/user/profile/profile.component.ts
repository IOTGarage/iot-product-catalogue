import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../../services/authentication/auth.service";
import { Router } from '@angular/router';
import { User } from "../../../models/user";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: User;

  constructor(
    private authService: AuthService) { }

  ngOnInit() {
    this.getAccountDetails();
  }

  getAccountDetails () {
    this.authService.getProfile().subscribe(profile => {
        this.user = profile.user;
      },
      err => {
        console.log(err);
        return false;
      });
  }

}
