# Olympus

Olympus is a crowdsourcing platform for analysing IoT solutions. 

 1. Admins can create base records (device templates) for each desirable device.
 2. Crowdsourcers can collectively document IoT device features and details.
 3. Admins then create a master copy of the device by merging the editions.
 4. Students can then view and compare devices.

## [Architecture](https://gitlab.cs.cf.ac.uk/c1518562/crowdsourcing-app/blob/master/architecture.png)


### Languages used


#### Scripts
- TypeScript - Angular (Frontend)
- JS - NodeJS/Express (Backend)


#### HTML
- HTML, SASS (CSS)


# Installation

 - Install [Node](https://nodejs.org/en/) (version used: 10.15.3)
 

In in root dir (CMD):
- install the Angular CLI globally: 
 `npm i @angular/cli -g`

- install node packages (CMD):
 `npm install`
 

In angular-src folder (CMD):
 - install node packages: `npm install`
 - build the application: `ng build`


# Run application


## Backend server
In CMD (root of project): `npm run start`

Go to http://localhost:3000


# Development

 - Run and track NodeJS/Express changes
`nodemon` in root dir

- Run and track changes to Angular
`ng serve` in angular-src folder



# Directory
## Root
- `app.js` - server code
- `config` - folder containing server-side configuration files
- `angular-src` - folder containing frontend code
- `models` - folder containing database models
- `routes` - folder containing routing files for server

## angular-src
- `src` - folder containing all source code
- `e2e` - end to end testing **(not covered)**

### src
- `app` - contains all code
- `assets` - contains all assets used in application
- `environments` - **(not covered)**
- `sass` - folder containing global sass files

#### app
- `components` - folder containing reusable components' code
- `guards` - folder containing route guards
- `models` - common object models
- `services` - folder containing services that connect the frontend to backend
- `app.component.*` - files related to the application's entry point