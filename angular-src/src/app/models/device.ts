export class Device {
  _id: String;
  name: String;
  price: String;
  image: String;
  description: String;
  creator: String;
  manufacturer: String;
  technology: any;
  readyToMerge: boolean = false;
  revisions: any = [];
  tags: any = [];
}
