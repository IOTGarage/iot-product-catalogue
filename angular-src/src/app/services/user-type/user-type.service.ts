import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserTypeService {

  constructor() { }

  getUserType() {
    return JSON.parse(localStorage.getItem('user')).userType.toUpperCase();
  }
}
