import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { UploadFileService } from "../../../services/upload-file/upload-file.service";

class FileSnippet {
  status: String = 'INIT';
  shortcut: number = 0;

  constructor(public src: String, public file: File) {}
}

@Component({
  selector: 'app-audio-upload',
  templateUrl: './audio-upload.component.html',
  styleUrls: ['./audio-upload.component.scss']
})
export class AudioUploadComponent implements OnInit {
  @Output() audioUploaded = new EventEmitter();
  @Output() audioError = new EventEmitter();

  selectedAudio: FileSnippet;

  constructor(
    private uploadService: UploadFileService) { }

  ngOnInit() {
  }

  onSuccess(res) {
    res.file.shortcut = this.selectedAudio.shortcut;
    this.selectedAudio.status = 'OK';
    this.audioUploaded.emit(res);
  }

  onFailure(res) {
    this.selectedAudio.status = 'FAIL';
    this.audioError.emit(res);
  }

  processFile(input: any) {
    this.selectedAudio = null;
    const file: File = input.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      this.selectedAudio = new FileSnippet(event.target.result, file)
    });

    reader.readAsDataURL(file);
  }

  uploadAudio() {
    let audio = document.getElementById("audioEvidenceSelector") as HTMLAudioElement;
    this.selectedAudio.shortcut = audio.currentTime;

    if (this.selectedAudio) {
      const formData = {
        audio: this.selectedAudio.file
      };

      this.uploadService.uploadAudio(formData).subscribe(
        (response) => {
          if (response.success) {
            this.onSuccess(response);
          }
          else {
            this.onFailure(response);
          }},
        err => {
          this.onFailure(err);
        })
    }
  }

}
