import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MergeEditionsComponent } from './merge-editions.component';

describe('MergeEditionsComponent', () => {
  let component: MergeEditionsComponent;
  let fixture: ComponentFixture<MergeEditionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MergeEditionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MergeEditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
