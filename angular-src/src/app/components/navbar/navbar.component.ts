import { Component, OnInit } from '@angular/core';
import { AuthService} from "../../services/authentication/auth.service";
import { UserTypeService } from "../../services/user-type/user-type.service";
import { Router } from '@angular/router';
import { FlashMessagesService} from "angular2-flash-messages";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  constructor(
    private authService: AuthService,
    private userTypeService: UserTypeService,
    private router: Router,
    private flashMessage: FlashMessagesService
  ) { }

  ngOnInit() {
  }

  onLogoutClick () {
    this.authService.logout();
    this.flashMessage.show('You are logged out', {
      cssClass: 'alert-success',
      timeout: 2000
    });

    this.router.navigate(['/login']);
    return false;
  }

}
