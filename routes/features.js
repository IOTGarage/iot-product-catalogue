const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

// Feature schema
const FeatureSchema = mongoose.Schema({
    category: String,
    listOfFeatures : [{
        type: String
    }]
});

const Feature = mongoose.model('Feature', FeatureSchema);

async function addMultipleFeatures (features) {
    let numberOfUpdates = 0;
    let numberOfCreations = 0;

    // split array of categories objects, and update/create for each object.
    for (let i = 0; i < features.length; i++) {
        const newFeatures = features[i];
        const category = newFeatures.category;
        const listOfFeatures = newFeatures.listOfFeatures;

        const foundCategory = await Feature.findOne({category: category});

        // if category already exists, update its list of features
        if (foundCategory) {
            // set to ensure values are unique
            let uniqueFeatures = new Set(foundCategory.listOfFeatures);

            for (let i = 0; i < listOfFeatures.length; i++) {
                uniqueFeatures.add(listOfFeatures[i]);
            }

            // need to convert back to array for Mongo
            foundCategory.listOfFeatures = Array.from(uniqueFeatures);
            foundCategory.save();
            numberOfUpdates++;
        }

        else {
            // create new category entry if category does not exist
            const newEntry = new Feature ({
                category: category,
                listOfFeatures: listOfFeatures
            });
            newEntry.save();
            numberOfCreations++;
        }
    }

    if (numberOfCreations !== 0 || numberOfUpdates !== 0) {
        return {success: true, msg: 'Features added successfully'};
    }
}

async function addFeature (tech) {
    let response = {success: false, msg: 'There was an issue'};

    const category = tech.category;
    const feature = tech.feature;

    const foundCategory = await Feature.findOne({category: category});

    // if category already exists, update its list of features
    if (foundCategory) {
        // set to ensure values are unique
        let uniqueFeatures = new Set(foundCategory.listOfFeatures);
        uniqueFeatures.add(feature);


        // need to convert back to array for Mongo
        foundCategory.listOfFeatures = Array.from(uniqueFeatures);
        foundCategory.save();
        response = {success: true, msg: 'Feature added to existing category'};
    }

    else {
        // create new category entry if category does not exist
        const newEntry = new Feature ({
            category: category,
            listOfFeatures: [feature]
        });
        newEntry.save();
        response = {success: true, msg: 'New Category and feature added'};
    }

    return response;
}

// add category and/or feature
router.post('/create', (req, res, next) => {
    const tech = {
        category: req.body.category,
        feature: req.body.feature
    };

    addFeature(tech).then(function (response) {
        res.json(response)
    });
});

// retrieve all features/categories
router.get('/all', (req, res, next) => {
    Feature.find({}, function (err, features) {
        if (err) {
            res.json({success: false, msg: err})
        }

        else if (features.length === 0) {
            res.json({success: false, msg: 'No features found'});
        }

        else {
            res.send({success: true, features: features});
        }
    })
});

module.exports = router;
