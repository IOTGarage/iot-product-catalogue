import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from "@angular/common/http";
import { HttpModule } from '@angular/http';
import 'hammerjs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/user/login/login.component';
import { RegisterComponent } from './components/user/register/register.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProfileComponent } from './components/user/profile/profile.component';
import { LogRegTabsComponent } from './components/user/log-reg-tabs/log-reg-tabs.component';
import { LogRegBannerComponent } from './components/user/log-reg-banner/log-reg-banner.component';
import { AddDeviceComponent } from './components/device/add-device/add-device.component';
import { ViewDeviceComponent } from './components/device/view-device/view-device.component';
import { ImageUploadComponent } from './components/uploads/image-upload/image-upload.component';
import { VideoUploadComponent } from './components/uploads/video-upload/video-upload.component';
import { PdfUploadComponent } from './components/uploads/pdf-upload/pdf-upload.component';
import { PdfViewerComponent } from 'ng2-pdf-viewer';
import { ViewPdfComponent } from './components/uploads/view-pdf/view-pdf.component';
import { AudioUploadComponent } from './components/uploads/audio-upload/audio-upload.component';
import { EvidenceViewerModalComponent } from './components/evidence-viewer-modal/evidence-viewer-modal.component';
import { CreateRevisionComponent } from './components/device/create-revision/create-revision.component';
import { MergeEditionsComponent } from './components/device/merge-editions/merge-editions.component';
import { CompareDevicesComponent } from './components/device/compare-devices/compare-devices.component';

import { ValidateService } from './services/validation/validate.service';
import { AuthService } from './services/authentication/auth.service';
import { FlashMessagesModule} from "angular2-flash-messages";
import { AuthGuard } from "./guards/auth.guard";
import { UploadFileService } from "./services/upload-file/upload-file.service";
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatTooltipModule } from '@angular/material/tooltip';
import { UserTypeService } from "./services/user-type/user-type.service";
import { CreateFeatureComponent } from './components/create-feature/create-feature.component';



const appRoutes: Routes = [
  {path: '', component: DashboardComponent, canActivate:[AuthGuard]},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {path: 'profile', component: ProfileComponent, canActivate:[AuthGuard]},
  {path: 'create-device', component: AddDeviceComponent, canActivate:[AuthGuard]},
  {path: 'view-device/:id', component: ViewDeviceComponent, canActivate:[AuthGuard]},
  {path: 'create-device-revision/:id', component: CreateRevisionComponent, canActivate:[AuthGuard]},
  {path: 'merge-device/:id', component: MergeEditionsComponent, canActivate:[AuthGuard]},
  {path: 'compare-device/:id1/:id2/:id3/:id4/:id5/:id6', component: CompareDevicesComponent, canActivate:[AuthGuard]},
  {path: 'create-feature', component: CreateFeatureComponent, canActivate:[AuthGuard]}
];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    ProfileComponent,
    LogRegTabsComponent,
    LogRegBannerComponent,
    AddDeviceComponent,
    ViewDeviceComponent,
    ImageUploadComponent,
    VideoUploadComponent,
    PdfUploadComponent,
    PdfViewerComponent,
    ViewPdfComponent,
    AudioUploadComponent,
    MergeEditionsComponent,
    EvidenceViewerModalComponent,
    CreateRevisionComponent,
    CompareDevicesComponent,
    CreateFeatureComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    FlashMessagesModule.forRoot(),
    HttpClientModule,
    HttpModule,
    ImageCropperModule,
    NgbModule,
    MatTooltipModule,
    BrowserAnimationsModule
  ],
  providers: [ValidateService, AuthService, AuthGuard, UploadFileService, UserTypeService],
  bootstrap: [AppComponent],
  entryComponents: [EvidenceViewerModalComponent]
})
export class AppModule { }
