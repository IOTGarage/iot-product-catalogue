import { Component, OnInit } from '@angular/core';
import { ValidateService} from "../../../services/validation/validate.service";
import { AuthService} from "../../../services/authentication/auth.service";
import { FlashMessagesService } from "angular2-flash-messages";
import { Router} from "@angular/router";
import {User} from "../../../models/user";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  user: User = new User();

  constructor(
    private validateService: ValidateService,
    private flashMessage: FlashMessagesService,
    private authService: AuthService,
    private router: Router ) { }

  ngOnInit() {
  }

  onRegisterSubmit () {
    // Validate email
    if (!ValidateService.validateEmail(this.user.email)) {
      this.flashMessage.show('Invalid email', {cssClass: 'alert-danger', timeout: 3000});
      return false;
    }

    // Register user
    this.authService.registerUser(this.user).subscribe(data => {
      if (data.success) {
        this.flashMessage.show('You are now registered and can log in', {cssClass: 'alert-success', timeout: 3000});
        this.router.navigate(['/login']);
      }

      else {
        this.flashMessage.show('Something went wrong', {cssClass: 'alert-danger', timeout: 3000});
        this.router.navigate(['/register']);
      }
    });
  }

}
