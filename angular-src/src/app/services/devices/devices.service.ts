import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import {Headers, Http} from "@angular/http";

@Injectable({
  providedIn: 'root'
})
export class DevicesService {
  constructor(private http:Http) { }

  createDevice (device) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('/devices/add', device, {headers: headers})
      .pipe(map(res => res.json()));
  }

  getDeviceById (id) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.get(`/devices/id/${id}`, {headers: headers})
        .pipe(map(res => res.json()));
  }

  getDeviceByIds (ids) {
    return this.http.get(`/devices/ids/${ids[0]}/${ids[1]}/${ids[2]}/${ids[3]}/${ids[4]}/${ids[5]}`)
      .pipe(map(res => res.json()));
  }

  getAllDevices () {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.get('/devices/all', {headers: headers})
      .pipe(map(res => res.json()));
  }

  createRevision (revision) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('/devices/add-revision', revision, {headers: headers})
      .pipe(map(res => res.json()));
  }

  mergeDevice(update) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('/devices/merge', update, {headers: headers})
      .pipe(map(res => res.json()));
  }

  deleteById(id) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('/devices/delete', id)
      .pipe(map(res => res.json()));
  }
}
