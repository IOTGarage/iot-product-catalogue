import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { UploadFileService } from "../../../services/upload-file/upload-file.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

class FileSnippet {
  status: String = 'INIT';

  constructor(public src: String, public file: File) {}
}

@Component({
  selector: 'app-pdf-upload',
  templateUrl: './pdf-upload.component.html',
  styleUrls: ['./pdf-upload.component.scss']
})
export class PdfUploadComponent implements OnInit {
  @Output() pdfUploaded = new EventEmitter();
  @Output() pdfError = new EventEmitter();

  selectedPDF: FileSnippet;
  pageNumber: number = 1;
  totalPages: number;
  isLoaded: boolean = false;

  constructor(
    private uploadService: UploadFileService,
    private modalService: NgbModal) { }

  ngOnInit() {
  }

  toggleIsLoaded(response) {
    this.isLoaded = response;
  }

  onPageSelect(response) {
    this.pageNumber = response;
  }

  selectedPdfExists () {
    return !!(this.selectedPDF && this.selectedPDF.src);
  }

  openModal(content) {
    this.modalService.open(content, { size: 'lg', centered: true });
  }

  onSuccess(res) {
    res.file.shortcut = this.pageNumber;
    this.selectedPDF.status = 'OK';
    this.pdfUploaded.emit(res);
  }

  onFailure(res) {
    this.selectedPDF.status = 'FAIL';
    this.pdfError.emit(res);
  }

  resetVariables() {
    this.selectedPDF = null;
    this.pageNumber = 1;
    this.totalPages = null;
    this.isLoaded = false;
  }

  processFile(input: any) {
    this.resetVariables();

    const file: File = input.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      this.selectedPDF = new FileSnippet(event.target.result, file);
    });

    reader.readAsDataURL(file);
  }

  uploadPDF() {
    if (this.selectedPDF) {
      const formData = {
        pdf: this.selectedPDF.file
      };

      this.uploadService.uploadPdf(formData).subscribe(
        (response) => {
          if (response.success) {
            this.onSuccess(response);
          }
          else {
            this.onFailure(response);
          }},
        err => {
          this.onFailure(err);
        })
    }
  }
}

