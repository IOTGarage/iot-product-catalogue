import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Device} from "../../../models/device";
import {DevicesService} from "../../../services/devices/devices.service";

@Component({
  selector: 'app-compare-devices',
  templateUrl: './compare-devices.component.html',
  styleUrls: ['./compare-devices.component.scss']
})
export class CompareDevicesComponent implements OnInit {
  devices: Device[] = [];

  deviceImages: any = [];
  deviceNames: any = [];
  deviceManufacturers: any = [];
  devicePrices: any = [];
  deviceDescriptions: any = [];
  featureDeviceMap: any = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private deviceService: DevicesService
  ) {
  }

  ngOnInit() {
    this.initDevices();
  }

  loadDevices(ids) {
    const self = this;

    self.deviceService.getDeviceByIds(ids).subscribe(response => {
        if (response.success) {
          response.devices.forEach(function (device) {
            self.devices.push(device);
          })
        }

        else {
          console.log(response.msg);
          return false;
        }
      },
      err => {
        console.log(err);
        return false;
    }, () => {
      this.createFeatureDeviceMap();
      this.createListOfDeviceDetails();
      });
  }

  initDevices() {
    this.route.params.subscribe(params => {
        const ids = Object.values(params);
        this.loadDevices(ids);
      },
      err => {
        console.log(err);
        return false;
      });
  }

  createFeatureDeviceMap() {
    const self = this;

    this.devices.forEach(function (device) {
      let categoryFound = false;

      // for each category in features
      if (device.technology) {
        device.technology.forEach(function (tech) {

          // check if exists in featureDeviceMap
          self.featureDeviceMap.forEach(function (mapCategory) {

            if (tech.category === mapCategory.category) {
              categoryFound = true;

              // if exists, for each feature in category
              tech.features.forEach(function (feature) {
                let featureFound = false;

                // check if feature exists
                mapCategory.features.forEach(function (mapFeature) {
                  if (mapFeature.feature === feature.feature) {
                    // if exists, add device._id to array
                    mapFeature.devices.push(device._id);
                    featureFound = true;
                  }
                });

                if (!featureFound) {
                  let newFeature = {
                    feature: feature.feature,
                    devices: [device._id]
                  };

                  mapCategory.features.push(newFeature);
                }
              });

            }
          });

          if (!categoryFound) {
            let newCategory = {
              category: tech.category,
              features: []
            };

            tech.features.forEach(function (feature) {
              let newFeature = {
                feature: feature.feature,
                devices: [device._id]
              };

              newCategory.features.push(newFeature);
            });

            // else, create new category key, and create array
            self.featureDeviceMap.push(newCategory);
          }
        })
      }
    });
  }

  createListOfDeviceDetails() {
    const self = this;

    self.devices.forEach(function (device) {
      self.deviceImages.push(device.image);
      self.deviceNames.push(device.name);
      self.deviceManufacturers.push(device.manufacturer);
      self.devicePrices.push(device.price);
      self.deviceDescriptions.push(device.description);
    });
  }

  toDashboard() {
    this.router.navigate(['/']);
  }

  toDevice(i) {
    this.router.navigate([`/view-device/${this.devices[i]._id}`]);
  }

  hasFeature(i, devicesWithFeature) {
    return devicesWithFeature.indexOf(this.devices[i]._id) !== -1;
  }

  isDevices(num) {
    return num === this.devices.length
  }

}
