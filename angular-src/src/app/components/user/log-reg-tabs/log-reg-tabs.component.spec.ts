import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogRegTabsComponent } from './log-reg-tabs.component';

describe('LogRegTabsComponent', () => {
  let component: LogRegTabsComponent;
  let fixture: ComponentFixture<LogRegTabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogRegTabsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogRegTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
