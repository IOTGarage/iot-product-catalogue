export class Feature {
  title: String;
  origin: String;
  purpose: String;
  src: String;
  thumbnail: String;
  evidenceType: String;
  shortcut: number = 0;
}
