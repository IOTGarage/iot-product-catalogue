import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Http, Headers } from "@angular/http";

@Injectable({
  providedIn: 'root'
})
export class FeaturesService {
  constructor(private http:Http) { }

  addFeature (features) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('/features/create', features, {headers: headers})
      .pipe(map(res => res.json()));
  }

  getFeatures () {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get('/features/all', {headers: headers})
      .pipe(map(res => res.json()));
  }

}
