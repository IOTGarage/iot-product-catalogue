import { Component, OnInit } from '@angular/core';
import { AuthService} from "../../../services/authentication/auth.service";
import { Router } from '@angular/router';
import { FlashMessagesService} from "angular2-flash-messages";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username: String;
  password: String;

  constructor(
    private authService: AuthService,
    private router: Router,
    private flashMessage: FlashMessagesService
  ) { }

  ngOnInit() {
  }

  onLoginSubmit () {
    if (this.username && this.password) {
     const user = {
       username: this.username,
       password: this.password
     };

     this.authService.authenticateUser(user).subscribe(data => {
       if (data.success) {
         this.authService.storeUserData(data.token, data.user);
         this.flashMessage.show('You are now logged in', {
           cssClass: 'alert-success',
           timeout: 2000
         });

         this.router.navigate(['/'])
       }
       else {
         this.flashMessage.show(data.msg, {
           cssClass: 'alert-danger',
           timeout: 2000
         });

         this.router.navigate(['/login'])
       }
     });
    }
    else {
      this.flashMessage.show('Please fill in the form.', {
          cssClass: 'alert-danger',
          timeout: 2000
        });

      this.router.navigate(['/login']);
    }
  }
}
