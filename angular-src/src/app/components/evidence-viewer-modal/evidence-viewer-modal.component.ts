import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-evidence-viewer-modal',
  templateUrl: './evidence-viewer-modal.component.html',
  styleUrls: ['./evidence-viewer-modal.component.scss']
})
export class EvidenceViewerModalComponent {

  @Input() title;
  @Input() src;
  @Input() type;
  @Input() shortcut;

  constructor(public activeModal: NgbActiveModal) {}

  seekVideo() {
    let vid = document.getElementById("evidence-video") as HTMLVideoElement;
    vid.currentTime = this.shortcut;
  }

  seekAudio() {
    let audio = document.getElementById("evidence-audio") as HTMLAudioElement;
    audio.currentTime = this.shortcut;
  }

  is(type) {
    return type === this.type;
  }

}
