import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogRegBannerComponent } from './log-reg-banner.component';

describe('LogRegBannerComponent', () => {
  let component: LogRegBannerComponent;
  let fixture: ComponentFixture<LogRegBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogRegBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogRegBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
