import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Device } from "../../../models/device";
import { DevicesService } from "../../../services/devices/devices.service";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EvidenceViewerModalComponent } from "../../evidence-viewer-modal/evidence-viewer-modal.component";
import { FeaturesService } from "../../../services/devices/features.service";
import { FlashMessagesService } from "angular2-flash-messages";

@Component({
  selector: 'app-merge-editions',
  templateUrl: './merge-editions.component.html',
  styleUrls: ['./merge-editions.component.scss']
})
export class MergeEditionsComponent implements OnInit {
  device: Device = new Device();
  predefinedFeatures: any = [];

  imageOptions: any = [];
  priceOptions: any = [];
  descriptionOptions: any = [];
  featuresOptions: any = [];

  areImageOptions: boolean = false;
  arePriceOptions: boolean = false;
  areDescriptionOptions: boolean = false;
  areFeaturesOptions: boolean = false;

  chosenCategory: String;
  selectedFeatureTitle: String;
  selectedFeatureEditions: any;
  isFeatureShown: boolean = false;

  editionSelection: object;
  isEditionSelected: boolean = false;
  anyFeatureConfirmed: boolean = false;

  stage: number = 1;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private deviceService: DevicesService,
    private modalService: NgbModal,
    private featuresService: FeaturesService,
    private flashMessage: FlashMessagesService
  ) { }

  ngOnInit() {
    this.loadDevice();
    this.loadFeaturesFromDb();
  }

  loadDevice() {
    this.route.params.subscribe(params =>
      {
        this.device._id = params.id;
      },
      err => {
        console.log(err);
        return false;
      });

    this.deviceService.getDeviceById(this.device._id).subscribe(response => {
        if (response.success) {
          this.device = response.device;

          if (response.device.technology === undefined) {
            this.device.technology = [];
          }

          this.initialiseDeviceDetails();
          this.createOptions();
        }

        else {
          console.log(response.device.msg);
          return false;
        }
      },
      err => {
        console.log(err);
        return false;
      },
      () => {
      console.log(this.device.technology);
      });
  }

  loadFeaturesFromDb () {
    this.featuresService.getFeatures().subscribe(response => {
        if (response.success) {
          response.features.forEach((element) => {
            this.predefinedFeatures.push({
              category: element.category,
              listOfFeatures: element.listOfFeatures
            });
          });
        }

        else {
          console.log('No predefined features');
        }},
      err => {
        console.log(err);
        return false;
      });
  }

  openMergeModal(content) {
    this.modalService.open(content, { centered: true });
  }

  openModal(evidence) {
    const modalRef = this.modalService.open(EvidenceViewerModalComponent, {size: 'lg'});
    modalRef.componentInstance.title = evidence.title;
    modalRef.componentInstance.src = evidence.src;
    modalRef.componentInstance.type = evidence.type;
    modalRef.componentInstance.shortcut = evidence.shortcut;
  }

  initialiseDeviceDetails() {
    if (this.device.price === undefined || !this.device.price) {
      this.device.price = 'N/A';
      this.device.description = ' ';
    }
  }

  isStage(stage) {
    return this.stage === stage;
  }

  createImageOptions(el) {
    if (el.image) {
      this.imageOptions.push({user: el.creator, image: el.image});
    }
  }

  createPriceOptions(el) {
    const originalPrice = this.device.price;

    if (originalPrice !== el.price) {
      this.priceOptions.push({user: el.creator, price: el.price})
    }
  }

  createDescriptionOptions(el) {
    const originalDescription = this.device.description;

    if (originalDescription !== el.description) {
      this.descriptionOptions.push({user: el.creator, description: el.description})
    }
  }

  createNewFeatures(category) {
    let newCategory = category.category;
    let newFeatures = [];

    category.features.forEach(function (feat) {
      let newFeat = {};
      newFeat['feature'] = feat.feature;
      newFeat['revisions'] = [feat.evidence];
      newFeatures.push(newFeat);
    });

    this.featuresOptions.push({category: newCategory, features: newFeatures});
  }

  createFeatureOptions(revision) {
    const self = this;

    revision.technology.forEach(function (revisionCategory) {
      if (self.featuresOptions.length > 0) {
        let categoryFound = false;

        self.featuresOptions.forEach(function (categoryOption) {
          // if category doesn't exist in featureOptions, create feature array for each revision
          if (revisionCategory.category === categoryOption.category) {
            categoryFound = true;

            revisionCategory.features.forEach(function (featureEdition) {
              // check if feature exists
              let featureFound = false;

              categoryOption.features.forEach(function (featureOption) {
                if (featureEdition.feature === featureOption.feature) {
                  featureOption.revisions.push(featureEdition.evidence);
                  featureFound = true;
                }
              });

              if (!featureFound) {
                let newFeat = {};
                newFeat['feature'] = featureEdition.feature;
                newFeat['revisions'] = [featureEdition.evidence];
                categoryOption.features.push(newFeat);
              }
            });
          }
        });

        if (!categoryFound) {
          self.createNewFeatures(revisionCategory);
        }
      }

      else {
        self.createNewFeatures(revisionCategory)
      }
      });
  }

  initImageOptions() {
    const originalImage = this.device.image;

    this.imageOptions.push({user: this.device.creator, image: originalImage});
  }

  initPriceOptions() {
    const originalPrice = this.device.price;
    let oldPrice = false;

    if (originalPrice !== 'N/A') {
      this.priceOptions.push({user: this.device.creator, price: originalPrice});
      oldPrice = true;
    }

    return oldPrice;
  }

  initDescriptionOptions() {
    const originalDescription = this.device.description;
    let oldDescription = false;

    if (originalDescription !== ' ') {
      this.descriptionOptions.push({user: this.device.creator, description: originalDescription});
      oldDescription = true;
    }

    return oldDescription;
  }

  initFeatureOptions() {
    if (this.device.technology.length !== 0) {
      const self = this;

      this.device.technology.forEach(function (category) {
        const newCategory = {
          category: category.category,
          features: []
        };

        category.features.forEach(function (feature) {
          const newFeature = {
            feature: feature.feature,
            revisions: [feature.evidence]
          };

          newCategory.features.push(newFeature);
        });

        self.featuresOptions.push(newCategory)
      })
    }
  }

  setAreImageOptions() {
    if (this.imageOptions.length > 1) {
      this.areImageOptions = true;
    }
  }

  setArePriceOptions(oldPrice) {
    if (this.priceOptions.length > 1 && oldPrice) {
      this.arePriceOptions = true;
    }
    else if (this.priceOptions.length > 0 && !oldPrice) {
      this.arePriceOptions = true;
    }
  }

  setAreDescriptionOptions(oldDescription) {
    if (this.descriptionOptions.length > 1 && oldDescription) {
      this.areDescriptionOptions = true;
    }

    else if (this.descriptionOptions.length > 0 && !oldDescription) {
      this.areDescriptionOptions = true;
    }
  }

  setAreFeatureOptions() {
    if (this.featuresOptions.length > 0) {
      this.areFeaturesOptions = true;
    }
  }

  createOptions() {
    const revisions = this.device.revisions;
    const self = this;

    this.initImageOptions();
    let oldPrice = this.initPriceOptions();
    let oldDescription = this.initDescriptionOptions();
    this.initFeatureOptions();

    revisions.forEach(function(el) {
      self.createImageOptions(el);
      self.createPriceOptions(el);
      self.createDescriptionOptions(el);
      self.createFeatureOptions(el)
    });

    this.setAreImageOptions();
    this.setArePriceOptions(oldPrice);
    this.setAreDescriptionOptions(oldDescription);
    this.setAreFeatureOptions();
  }

  selectBaseItem(detail, item) {
    this.device[detail] = item;
  }

  isChosen(detail, item) {
    return item === this.device[detail];
  }

  nextStage() {
    this.stage++;
  }

  previousStage() {
    this.stage--;
  }

  showFeatureEditions(feature, category) {
    if (this.selectedFeatureTitle === feature.feature) {
      this.isFeatureShown = !this.isFeatureShown;
    }

    else {
      this.chosenCategory = category;
      this.isFeatureShown = true;
      this.selectedFeatureTitle = feature.feature;
      this.selectedFeatureEditions = feature.revisions;
    }
  }

  onEditionSelection(edition) {
    this.editionSelection = edition;
    this.isEditionSelected = true;
  }

  getCategory(feature) {
    const featuresList = this.predefinedFeatures;

    for (let i = 0; i < featuresList.length; i++) {
      if (featuresList[i].listOfFeatures.indexOf(feature) !== -1) {
        return featuresList[i].category;
      }
    }
  }

  isInSelectedTechnology(feature) {
    const self = this;
    let found = false;

    this.device.technology.forEach(function (tech) {
      if (tech.category === self.getCategory(self.selectedFeatureTitle)) {
        tech.features.forEach(function (storedFeature) {
          if (storedFeature.evidence.src === feature.src) {
            found = true
          }
        })
      }
    });


    return found;
  }

  isEditionChosen(edition) {
    return JSON.stringify(edition) === JSON.stringify(this.editionSelection)
  }

  isEditionSaved(edition) {
    return this.isInSelectedTechnology(edition)
  }

  confirmEdition() {
    const self = this;
    let featureFound = false;
    let categoryFound = false;
    let categoryIndex;

    let confirmedFeature = {
      feature: this.selectedFeatureTitle,
      evidence: this.editionSelection
    };

    let categories = this.device.technology;

    for (let i = 0; i < categories.length; i++) {
      if (categories[i].category === self.chosenCategory) {
        categoryFound = true;
        categoryIndex = i;

        categories[i].features.forEach(function (feature) {
          if (feature.feature === confirmedFeature.feature) {
            feature.evidence = confirmedFeature.evidence;
            featureFound = true;
          }
        })
      }
    }

    if (categoryFound && !featureFound) {
      categories[categoryIndex].features.push(confirmedFeature);
    }

    else if (!categoryFound) {
      const confirmedCategory = {
        category: this.chosenCategory,
        features: [confirmedFeature]
      };

      categories.push(confirmedCategory)
    }

    this.isFeatureShown = false;
    this.isEditionSelected = false;
  }

  featureAvailable(feature, category) {
    let response = {
      text: '(Click to choose a version)',
      success: false
    };

    const self = this;

    this.device.technology.forEach(function (tech) {
      if (tech.category === category) {
        tech.features.forEach(function (features) {
          if (features.feature === feature) {
            response.text = 'Version selected';
            response.success = true;
            self.anyFeatureConfirmed = true;
          }
        })
      }
    });

    return response;
  }

  static is(format, type) {
    return format === type
  }

  merge() {
    this.createTags();
    this.device.revisions = [];
    this.device.readyToMerge = false;
    const device = {device: this.device};

    this.deviceService.mergeDevice(device).subscribe(response => {
        if (response.success) {
          this.flashMessage.show(response.msg, {cssClass: 'alert-success', timeout: 3000});
          this.router.navigate(['/']);
        }

        else {
          console.log(response.msg);
          return false;
        }

      },
      err => {
        console.log(err);
        return false;
      }
    );
  }

  createListOfCategories() {
    let listOfCategories = [];

    this.device.technology.forEach(function (category) {
      listOfCategories.push(category.category);
    });

    return listOfCategories;
  }

  createListOfFeatures() {
    let listOfFeatures = [];

    this.device.technology.forEach(function (category) {
      category.features.forEach(function (feature) {
        listOfFeatures.push(feature.feature);
      });
    });

    return listOfFeatures
  }

  createTags() {
    const categories = this.createListOfCategories();
    const features = this.createListOfFeatures();
    const self = this;

    this.device.tags = [this.device.name.toLowerCase(), this.device.manufacturer.toLowerCase()];

    categories.forEach(function (category) {
      self.device.tags.push(category.toLowerCase());
    });

    features.forEach(function (feature) {
      self.device.tags.push(feature.toLowerCase());
    })
  }
}
