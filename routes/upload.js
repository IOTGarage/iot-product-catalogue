const path = require('path');
const express = require('express');
const multer = require('multer');
const router = express.Router();
const fs = require('fs');
const ffmpegInstaller = require('@ffmpeg-installer/ffmpeg');
const ffmpeg = require('fluent-ffmpeg');

const imageTypes = {
    'image/jpeg': '.jpg',
    'image/png': '.png',
    'image/gif': '.gif'
};

// directory for uploaded device images
const devicePath = multer.diskStorage({
    destination: './public/uploads/devices',
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + imageTypes[file.mimetype]);
    }
});

// directory for uploaded device images
const imagePath = multer.diskStorage({
    destination: './public/uploads/evidence/images',
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + imageTypes[file.mimetype]);
    }
});

// directory for uploaded videos
const videoPath = multer.diskStorage({
    destination: './public/uploads/evidence/videos',
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});

// directory for uploaded PDFs
const pdfPath = multer.diskStorage({
    destination: './public/uploads/evidence/pdf',
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});

// directory for uploaded audio files
const audioPath = multer.diskStorage({
    destination: './public/uploads/evidence/audio',
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});

// Multer uploader for image
const uploadDeviceImage = multer({
    storage: devicePath,
    fileFilter: function (req, file, cb) {
        isImage(file, cb);
    }
}).single('device');

// Multer uploader for image
const uploadImage = multer({
    storage: imagePath,
    fileFilter: function (req, file, cb) {
        isImage(file, cb);
    }
});

// Multer uploader for video
const uploadVideo = multer({
    storage: videoPath,
    fileFilter: function (req, file, cb) {
        isVideo(file, cb);
    }
});

// Multer uploader for pdf
const uploadPdf = multer({
    storage: pdfPath,
    fileFilter: function (req, file, cb) {
        isPdf(file, cb);
    }
});

// Multer uploader for audio
const uploadAudio = multer({
    storage: audioPath,
    fileFilter: function (req, file, cb) {
        isAudio(file, cb);
    }
}).single('evidence');

// Checks file is an image
function isImage (file, cb) {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        return cb(null, true)
    }
    else {
        return cb('Invalid file type: Must be JPG/PNG', false);
    }
}

// Checks file is a video
function isVideo (file, cb) {
    // allowed file types
    const fileTypes = /mp4/;
    // check ext
    const extName = fileTypes.test(path.extname(file.originalname).toLowerCase());
    // check mime
    const mimetype = fileTypes.test(file.mimetype);

    if (extName && mimetype) {
        return cb(null, true)
    }
    else {
        cb('Invalid file type: Must be MP4');
    }
}

// Checks file is a pdf
function isPdf (file, cb) {
    if (file.mimetype === 'application\/pdf') {
        return cb(null, true)
    }
    else {
        cb('Invalid file type: Must be PDF');
    }
}

// Checks file is audio
function isAudio (file, cb) {
    // allowed file types
    const fileTypes = /wav|mp3|aac|audio\/mpeg|audio\/aac|/;
    // check ext
    const extName = fileTypes.test(path.extname(file.originalname).toLowerCase());
    // check mime
    const mimetype = fileTypes.test(file.mimetype);

    if (extName && mimetype) {
        return cb(null, true)
    }
    else {
        cb(file.mimetype);
    }
}

// route for uploading device image
router.post('/device', (req, res) => {
    uploadDeviceImage(req, res, (err) => {
        if (err) {
            res.json({success: false, msg: err});
        }
        else {
            res.json({success: true, file: `uploads/devices/${req.file.filename}`, msg: 'Image uploaded successfully'});
        }
    });
});

// route for uploading device image
router.post('/evidence/images', (req, res) => {
    uploadImage.array('evidence')(req, res, (err) => {
        if (err) {
            res.json({success: false, msg: err});
        }
        else {
            res.json({
                success: true,
                files: {
                    image: `uploads/evidence/images/${req.files[0].filename}`,
                    thumbnail: `uploads/evidence/images/${req.files[1].filename}`
                },
                type: 'image',
                msg: 'Images uploaded successfully'
            });
        }
    });
});

async function generateThumbnailFromVideo (path, file, timestamp, thumbnail) {
    const fullFilePath = path + file;

    ffmpeg.setFfmpegPath(ffmpegInstaller.path);

    return new Promise((resolve, reject) => {
        ffmpeg(fullFilePath)
            .on('end', function () {
                resolve();
            })
            .on('error', function (err) {
                reject({success: false, msg: err});
            })
            // take 2 screenshots at predefined timemarks and size
            .takeScreenshots({
                count: 1,
                timestamps: [timestamp],
                size: '300x169',
                folder: 'public/uploads/evidence/videos/',
                filename: thumbnail
            });
    });
}

// route for uploading video
router.post('/evidence/video', (req, res) => {
    uploadVideo.single('evidence')(req, res, (err) => {
        if (err) {
            res.json({success: false, msg: err});
        }
        else {
            const path = 'public/uploads/evidence/videos/';
            const file = req.file.filename;
            const timestamp = req.body.timestamp;
            const thumbnail = `screenshot-${Date.now()}`;

            generateThumbnailFromVideo(path, file, timestamp, thumbnail).then(() => {
                res.json({
                    success: true,
                    files: {
                        file: `uploads/evidence/videos/${req.file.filename}`,
                        thumbnail: `uploads/evidence/videos/${thumbnail}.png`,
                        shortcut: timestamp
                    },
                    type: 'video'
                });
            })
                .catch(error => res.json({success: false, msg: error}));
        }
    });

});


// route for uploading pdf
router.post('/evidence/pdf', (req, res) => {
    uploadPdf.single('evidence')(req, res, (err) => {
        if (err) {
            res.json({success: false, msg: err});
        }
        else {
            res.json({
                success: true,
                file: {file: `${'https://' + req.get('host')}/uploads/evidence/pdf/${req.file.filename}`},
                type: 'pdf'
            });
        }
    });
});

// route for uploading audio
router.post('/evidence/audio', (req, res) => {
    uploadAudio(req, res, (err) => {
        if (err) {
            res.json({success: false, msg: err});
        }
        else {
            res.json({success: true, file: {file: `uploads/evidence/audio/${req.file.filename}`}, type: 'audio', msg: 'Audio file uploaded successfully'});
        }
    });
});

function deleteFiles(files, callback){
    let i = files.length;
    files.forEach(function(filepath){
        const localPath = './public/' + filepath;
        fs.unlink(localPath, function(err) {
            i--;
            if (err) {
                callback(err);
                return;
            }
            else if (i <= 0) {
                callback(null);
            }
        });
    });
}

// deletes a photo
router.delete('/delete', (req, res) => {
    let files = req.body.filesToDelete;

    deleteFiles(files, function(err) {
        if (err) {
            console.log(err);
        }
        else {
            res.json({success: true, msg: 'All files deleted'});
        }
    });

});

module.exports = router;
