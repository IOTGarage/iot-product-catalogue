import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { DevicesService } from "../../../services/devices/devices.service";
import { FeaturesService } from "../../../services/devices/features.service";
import { UploadFileService } from "../../../services/upload-file/upload-file.service";

import { Feature } from "../../../models/feature";
import { Device } from "../../../models/device";
import { Revision } from "../../../models/revision";

import { FlashMessagesService } from "angular2-flash-messages";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { EvidenceViewerModalComponent } from "../../evidence-viewer-modal/evidence-viewer-modal.component";

@Component({
  selector: 'app-view-device',
  templateUrl: './view-device.component.html',
  styleUrls: ['./view-device.component.scss']
})

export class ViewDeviceComponent implements OnInit {
  // to populate and store device details
  device: Device = new Device();
  revision: Revision = new Revision();
  feature: Feature = new Feature();
  oldImage: String;

  instructions: String;

  featureButton: String = 'Add';

  // stores db features
  predefinedFeatures = [];
  // stores already chosen features for device
  chosenFeatures = [];
  featuresAdded: boolean = false;
  selectedFeature = '';

  // if feature has been clicked on from chosen features
  featureViewed: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private deviceService: DevicesService,
    private featuresService: FeaturesService,
    private uploadService: UploadFileService,
    private flashMessage: FlashMessagesService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.loadDevice();
    this.loadFeaturesFromDb();
  }

  openModal() {
    const modalRef = this.modalService.open(EvidenceViewerModalComponent, {size: 'lg'});
    modalRef.componentInstance.title = this.feature.title;
    modalRef.componentInstance.src = this.feature.src;
    modalRef.componentInstance.type = this.feature.evidenceType;
    modalRef.componentInstance.shortcut = this.feature.shortcut;
  }

  loadDevice () {
    this.route.params.subscribe(params =>
      {
        this.device._id = params.id;
        const userType = JSON.parse(localStorage.getItem('user')).userType;

        if (userType === 'crowdsourcer') {
          this.router.navigate([`/create-device-revision/${params.id}`]);
        }
      },
      err => {
        console.log(err);
        return false;
      });

    this.deviceService.getDeviceById(this.device._id).subscribe(response => {
      if (response.success) {
        this.device.image = response.device.image;
        this.oldImage = response.device.image;

        this.device.name = response.device.name;
        this.device.manufacturer = response.device.manufacturer;

        this.instructions = 'You cannot view any features';

        // if not template || template doesn't contain price
        if (response.device.price) {
          this.device.price = response.device.price;
          this.device.description = response.device.description;
          this.device.creator = response.device.creator;
          this.chosenFeatures = response.device.technology;
          this.featuresAdded = response.device.technology.length > 0;

          if (this.featuresAdded) {
            this.instructions = 'Select a feature from the left';
          }
        }
      }

      else {
        console.log(response.msg);
        return false;
      }


      },
      err => {
        console.log(err);
        return false;
      });
  }

  loadFeaturesFromDb () {
    this.featuresService.getFeatures().subscribe(response => {
        if (response.success) {
          response.features.forEach((element) => {
            this.predefinedFeatures.push({
              category: element.category,
              listOfFeatures: element.listOfFeatures
            });
          });
        }

        else {
          console.log('No predefined features');
        }},
      err => {
        console.log(err);
        return false;
      });
  }

  // populates addfeature section with data from feature section so user can edit
  populateAddFeatureSection (createdFeature) {
    this.feature.title = createdFeature.evidence.title;
    this.feature.origin = createdFeature.evidence.origin;
    this.feature.purpose = createdFeature.evidence.purpose;
    this.feature.src = createdFeature.evidence.src;
    this.feature.thumbnail = createdFeature.evidence.thumbnail;
    this.feature.evidenceType = createdFeature.evidence.type;
    this.feature.shortcut = createdFeature.evidence.shortcut;

    this.selectedFeature = createdFeature.feature;
  }

  onFeatureClick (feature) {
    this.featureViewed = true;
    this.featureButton = 'Update';
    this.populateAddFeatureSection(feature);
  }
}
