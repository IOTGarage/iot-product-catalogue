const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

// Device schema
const DeviceSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    creator: {
        type: String,
        required: true
    },
    creationTime: {
        type: Date,
        required: true,
        default: Date.now()
    },
    price: String,
    description: String,
    technology: {},
    manufacturer: String,
    readyToMerge: Boolean,
    revisions: Array,
    tags: Array
});

// const Device = module.exports = mongoose.model('Device', DeviceSchema);
const Device = mongoose.model('Device', DeviceSchema);

function addDevice (newDevice, callback) {
    newDevice.save(callback);
}

// add device
router.post('/add', (req, res, next) => {
    let newDevice = new Device({
        name: req.body.name,
        image: req.body.image,
        manufacturer: req.body.manufacturer,
        creator: req.body.creator,
        creationTime: new Date(),
        readyToMerge: req.body.readyToMerge,
        revisions: req.body.revisions,
        tags: req.body.tags
    });

    addDevice(newDevice, (err, device) => {
        if (err) {
            res.json({success: false, msg: err});
        }
        else {
            res.json({success: true, msg: 'Device added', id: device._id});
        }
    })
});

router.post('/add-revision', (req, res, next) => {
    const query = {
        '_id': req.body._id,
    };

    Device.update(query, {
        $set: {readyToMerge: true},
        $push: {revisions: req.body.revision}
        },

        (err, device) => {

        if (err) {
            res.json({success: false, msg: err})
        }
        else {
            res.json({success: true, msg: `${device.name} revision added and set to mergeable`})
        }
    })
});

router.post('/merge', (req, res, next) => {
    const query = {
        '_id': req.body.device._id,
    };

    Device.replaceOne(query, req.body.device,

        (err) => {

            if (err) {
                res.json({success: false, msg: err})
            }
            else {
                res.json({success: true, msg: `${req.body.device.name} has been merged successfully`})
            }
        })
});

// retrieve all devices
router.get('/all',(req, res, next) => {
    Device.find({}, function (err, devices) {
       if (err) {
           res.json({success: false, msg: err})
       }

       else if (!devices) {
            res.json({success: false, msg: 'No devices found'});
       }

       else {
           res.json({success: true, devices: devices});
       }
    })
});

// retrieve by name
router.get('/name',(req, res, next) => {
    const name = req.body.name;

    Device.find({name: name}, function(err, device){
        if (err) {
            res.json({success: false, msg: err});
        }

        else if (!device) {
            res.json({success: false, msg: 'Device not found'});
        }

        else {
            res.json({success: true, device: device});
        }
    })
});

// retrieve by _id
router.get('/id/:id',(req, res, next) => {
    const id = req.params.id;

    Device.findById(id, function(err, device){
        if (err) {
            res.json({success: false, msg: err});
        }

        else if (!device) {
            res.json({success: false, msg: 'Device not found'});
        }

        else {
            res.json({success: true, device: device});
        }
    })
});

router.get('/ids/:idOne/:idTwo/:idThree/:idFour/:idFive/:idSix', (req, res, next) => {
    let deviceIds = Object.values(req.params);

    deviceIds = deviceIds.filter(a => a !== '0');

    Device.find({
        '_id': { $in: deviceIds}
    }, function(err, devices){
        if (err) {
            res.json({success: false, msg: err})
        }

        else {
            res.json({success: true, devices: devices})
        }
    });
});

router.post('/delete', (req, res, next) => {
    Device.findByIdAndDelete(req.body.id, (err) => {
            if (err) {
                res.json({success: false, msg: err})
            }
            else {
                res.json({success: true, msg: `Device deleted`})
            }
        })
});

module.exports = router;
