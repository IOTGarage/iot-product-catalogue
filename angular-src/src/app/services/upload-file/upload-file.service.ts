import { Injectable } from '@angular/core';
import {Headers, Http, Response} from "@angular/http";
import { Observable } from "rxjs";
import {map} from "rxjs/operators";



@Injectable({
  providedIn: 'root'
})
export class UploadFileService {
  constructor(private http: Http) { }

  public uploadImage(request) {
    const formData = request.formData;

    if (request.upload === 'multiple') {
      return this.http.post('/upload/evidence/images', formData)
       .pipe(map(res => res.json()));
    }

    else if (request.upload === 'single') {
      return this.http.post('/upload/device', formData)
       .pipe(map(res => res.json()));
    }

  }

  public uploadVideo(file): Observable<String | any> {
    const formData = new FormData();

    formData.append('evidence', file.video);
    formData.append('timestamp', file.timestamp);

    return this.http.post('/upload/evidence/video', formData)
      .pipe(map(res => res.json()));
  }

  public uploadAudio(file): Observable<String | any> {
    const formData = new FormData();

    formData.append('evidence', file.audio);

    return this.http.post('/upload/evidence/audio', formData)
      .pipe(map(res => res.json()));
  }

  public uploadPdf(file): Observable<String | any> {
    const formData = new FormData();

    formData.append('evidence', file.pdf);

    return this.http.post('/upload/evidence/pdf', formData)
      .pipe(map(res => res.json()));
  }

  public delete(files) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.delete('/upload/delete', {headers: headers, body: files})
      .pipe(map(res => res.json()));
  }
}
