import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from "angular2-flash-messages";
import { Router } from "@angular/router";

import { DevicesService } from "../../../services/devices/devices.service";
import { UploadFileService } from "../../../services/upload-file/upload-file.service";

import { Device } from "../../../models/device";

@Component({
  selector: 'app-add-device',
  templateUrl: './add-device.component.html',
  styleUrls: ['./add-device.component.scss']
})
export class AddDeviceComponent implements OnInit {
  device: Device = new Device();

  constructor(
    private flashMessage: FlashMessagesService,
    private router: Router,
    private deviceService: DevicesService,
    private uploadService: UploadFileService
  ) { }

  ngOnInit() {
    this.device.creator = JSON.parse(localStorage.getItem('user')).username
  }

  onCreateSubmit () {
    this.createTags();

    this.deviceService.createDevice(this.device).subscribe(data => {
      if (data.success) {
        this.flashMessage.show('Device template created.', {cssClass: 'alert-success', timeout: 3000});
        this.router.navigate(['/']);
      }

      else {
        console.log(data.msg);
        this.flashMessage.show('Something went wrong', {cssClass: 'alert-danger', timeout: 3000});
      }
    });
  }

  handleImageDeviceUpload(res) {
    this.device.image = res.file;
    this.flashMessage.show(res.msg, {cssClass: 'alert-success', timeout: 3000});
  }

  handleUploadError(res) {
    this.flashMessage.show('Something went wrong', {cssClass: 'alert-danger', timeout: 3000});
  }

  deleteFiles(toDelete) {
    const files = {filesToDelete: toDelete};

    this.uploadService.delete(files).subscribe(response => {
        if (!response.success) {
          console.log(response.msg);
        }},
      err => {
        console.log(err);
        return false;
      });
  }

  changeDeviceImage() {
    this.deleteFiles([this.device.image]);
    this.device.image = null;
  }

  createTags() {
    this.device.tags.push(this.device.name.toLowerCase());
    this.device.tags.push(this.device.manufacturer.toLowerCase());
  }
}
