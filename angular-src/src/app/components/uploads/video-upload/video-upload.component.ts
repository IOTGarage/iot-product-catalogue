import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { UploadFileService } from "../../../services/upload-file/upload-file.service";

class FileSnippet {
  status: String = 'INIT';
  shortcut: number = 0;

  constructor(public src: String, public file: File) {}
}

@Component({
  selector: 'app-video-upload',
  templateUrl: './video-upload.component.html',
  styleUrls: ['./video-upload.component.scss']
})

export class VideoUploadComponent implements OnInit {
  @Output() videoUploaded = new EventEmitter();
  @Output() videoError = new EventEmitter();

  selectedVideo: FileSnippet;

  constructor(
    private uploadService: UploadFileService) { }

  ngOnInit() {
  }

  isStatus(status) {
    return this.selectedVideo.status === status;
  }

  onSuccess(res) {
    this.selectedVideo.status = 'OK';
    this.videoUploaded.emit(res);
  }

  onFailure(res) {
    this.selectedVideo.status = 'FAIL';
    this.videoError.emit(res);
  }

  processFile(input: any) {
    this.selectedVideo = null;
    const file: File = input.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      this.selectedVideo = new FileSnippet(event.target.result, file)
    });

    reader.readAsDataURL(file);
  }

  uploadVideo() {
    let vid = document.getElementById("videoEvidenceSelector") as HTMLVideoElement;
    this.selectedVideo.shortcut = vid.currentTime;

    if (this.selectedVideo) {
      const formData = {
        video: this.selectedVideo.file,
        timestamp: this.selectedVideo.shortcut
      };

      this.uploadService.uploadVideo(formData).subscribe(
        (response) => {
          if (response.success) {
            this.onSuccess(response);
          }
          else {
            this.onFailure(response);
          }},
          err => {
          this.onFailure(err);
        })
    }
  }

}
