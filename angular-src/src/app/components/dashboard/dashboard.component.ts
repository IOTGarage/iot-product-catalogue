import { Component, OnInit } from '@angular/core';
import { DevicesService } from "../../services/devices/devices.service";
import { Router } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { FlashMessagesService } from "angular2-flash-messages";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  devices: any = [];
  revisions: any;

  addButtonClicked: boolean = false;

  isAdmin: boolean;
  isCrowdsourcer: boolean;
  isStudent: boolean;

  toCompare: any = [];
  showCompare: boolean = false;

  query: String;
  queriedDevices: any = [];
  queried: boolean = false;

  maxCompare: number = 6;

  constructor(
    private devicesService: DevicesService,
    private router: Router,
    private modalService: NgbModal,
    private flashMessage: FlashMessagesService
  ) { }

  ngOnInit() {
    const user: String = JSON.parse(localStorage.getItem('user')).userType;
    this.isAdmin = user === 'admin';
    this.isCrowdsourcer = user === 'crowdsourcer';
    this.isStudent = user === 'student';

    this.loadAllDevices();

  }

  initQueriedDevices(devices) {
    this.queriedDevices = devices;
  }

  loadAllDevices() {
    this.devicesService.getAllDevices().subscribe(response => {
        if (response.success) {
          this.devices = response.devices;
          this.initQueriedDevices(response.devices);
        }

        else {
          console.log(response.msg);
          return false;
        }
      },
      err => {
        console.log(err);
        return false;
      });
  }

  viewDevice(id) {
    this.router.navigate([`/view-device/${id}`]);
  }

  mergeDevice(id) {
    this.router.navigate([`/merge-device/${id}`]);
  }

  openModal(content) {
    this.modalService.open(content, { centered: true });
  }

  loadDeviceAsAdmin(device, content) {
    if (device.readyToMerge) {
      this.openModal(content)
    }

    else {
      this.viewDevice(device._id)
    }
  }

  loadDeviceAsCrowdsourcer(device) {
    this.router.navigate([`/create-device-revision/${device._id}`]);
  }

  loadDeviceAsStudent(device) {
    this.viewDevice(device._id)
  }

  toggleAddMenu () {
    this.addButtonClicked = !this.addButtonClicked;
  }

  newFeatureClick () {
    this.router.navigate(['/create-feature']);
  }

  newDeviceClick () {
    this.router.navigate(['/create-device']);
  }

  addToCompare(event, device) {
    if(event.target.checked){
      if (this.toCompare.length !== this.maxCompare) {
        this.toCompare.push(device);
      }
      else {
        event.preventDefault()
      }
    }

    else {
      for (let i = 0; i < this.toCompare.length; i++) {
        if (this.toCompare[i]._id === device._id) {
          this.toCompare.splice(i, 1);
          break;
        }
      }
    }

    this.showCompare = this.toCompare.length > 0;
  }

  compareDevices() {
    const id1 = this.toCompare[0]._id;
    const id2 = this.toCompare[1]._id;

    let id3 = 0;
    let id4 = 0;
    let id5 = 0;
    let id6 = 0;

    if (this.toCompare.length > 2) {
      id3 = this.toCompare[2]._id;
    }

    if (this.toCompare.length > 3) {
      id4 = this.toCompare[3]._id;
    }

    if (this.toCompare.length > 4) {
      id5 = this.toCompare[4]._id;
    }

    if (this.toCompare.length > 5) {
      id6 = this.toCompare[5]._id;
    }

    this.router.navigate([`/compare-device/${id1}/${id2}/${id3}/${id4}/${id5}/${id6}`]);

  }

  search() {
    const self = this;

    if (this.query !== undefined && this.query !== '' && this.query !== null) {
      this.queriedDevices = [];

      this.query = this.query.toLowerCase();

      this.devices.forEach(function (device) {

        if (device.tags.indexOf(self.query) !== -1) {
          self.queriedDevices.push(device)
        }
      });

      this.queried = true;
    }
  }

  clearQuery() {
    this.query = null;
    this.initQueriedDevices(this.devices);
    this.queried = false;
  }

  deleteDevice(id) {
    for (let i = 0; i < this.devices.length; i++) {
      if (this.devices[i]._id === id) {
        this.devices.splice(i, 1);

        const apiData = {id: id};

        this.devicesService.deleteById(apiData).subscribe(response => {
            if (response.success) {
              this.flashMessage.show(response.msg, {cssClass: 'alert-success', timeout: 3000});
            }

            else {
              console.log(response.msg);
              return false;
            }

          },
          err => {
            console.log(err);
            return false;
          });

        break;
      }
    }
  }
}
