import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompareDevicesComponent } from './compare-devices.component';

describe('CompareDevicesComponent', () => {
  let component: CompareDevicesComponent;
  let fixture: ComponentFixture<CompareDevicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompareDevicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompareDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
