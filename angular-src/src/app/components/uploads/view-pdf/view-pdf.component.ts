import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-view-pdf',
  templateUrl: './view-pdf.component.html',
  styleUrls: ['./view-pdf.component.scss']
})
export class ViewPdfComponent implements OnInit {
  @Input() inputPage: number;
  @Input() inputSrc: String;

  @Output() isLoaded = new EventEmitter();
  @Output() pageNumber = new EventEmitter();

  totalPages: number;
  page: number = 1;
  loaded: boolean = false;

  constructor() { }

  ngOnInit() {
    if (this.inputPage) {
      this.page = this.inputPage
    }
  }

  prevPage() {
    this.page--;
  }

  nextPage() {
    this.page++;
  }

  afterLoadComplete(pdfData: any) {
    this.totalPages = pdfData.numPages;
    this.isLoaded.emit(true);
    this.loaded = true;
  }

  pageChanged() {
    this.pageNumber.emit(this.page);
  }
}

