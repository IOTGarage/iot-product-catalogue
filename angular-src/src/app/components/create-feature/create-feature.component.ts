import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from "angular2-flash-messages";
import { Router } from "@angular/router";
import { FeaturesService } from "../../services/devices/features.service";

@Component({
  selector: 'app-create-feature',
  templateUrl: './create-feature.component.html',
  styleUrls: ['./create-feature.component.scss']
})
export class CreateFeatureComponent implements OnInit {
  predefinedCategories: any = [];
  selectedCategory: String = '';

  featureCategory: String;
  featureName: String;

  constructor(
    private flashMessage: FlashMessagesService,
    private router: Router,
    private featuresService: FeaturesService) { }

  ngOnInit() {
    this.loadFeaturesFromDb()
  }

  loadFeaturesFromDb() {
    this.featuresService.getFeatures().subscribe(response => {
        if (response.success) {
          response.features.forEach((element) => {
            this.predefinedCategories.push({
              category: element.category,
              listOfFeatures: element.listOfFeatures
            });
          });
        }

        else {
          console.log('No predefined features');
        }},
      err => {
        console.log(err);
        return false;
      });
  }

  onCreateSubmit() {
    const feature = {
      category: this.featureCategory,
      feature: this.featureName
    };

    this.featuresService.addFeature(feature).subscribe(response => {
      if (response.success) {
        this.flashMessage.show(response.msg, {cssClass: 'alert-success', timeout: 3000});
        this.router.navigate(['/']);
      }

      else {
        console.log(response.msg);
        this.flashMessage.show(response.msg, {cssClass: 'alert-danger', timeout: 3000});
      }
    });

  }

  onCategorySelect() {
    if (this.selectedCategory !== 'Create New' && this.selectedCategory !== '') {
      this.featureCategory = this.selectedCategory;
    }

    else {
      this.featureCategory = ''
    }
  }

}
