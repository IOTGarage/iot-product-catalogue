import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { UploadFileService } from "../../../services/upload-file/upload-file.service";
import { FlashMessagesService } from "angular2-flash-messages";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

class FileSnippet {
  static readonly IMAGE_SIZE = {width: 250, height: 250};
  pending: boolean = false;
  status: String = 'INIT';

  constructor(public src: String, public file: File) {}
}

@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.scss']
})

export class ImageUploadComponent implements OnInit {
  @Input() aspectRatio: number = 4/3;
  @Output() imageUploaded = new EventEmitter();
  @Output() imageError = new EventEmitter();

  originalImage: FileSnippet;
  croppedImage: FileSnippet;
  imageChangedEvent: any;
  modalTitle: String;

  constructor(
    private uploadService: UploadFileService,
    private flashMessage: FlashMessagesService,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.modalTitle = this.aspectRatio === 4/3 ? 'Please select a region for the thumbnail' : 'Please select the region you wish to choose';
  }

  private onSuccess(res) {
    this.croppedImage.pending = false;
    this.croppedImage.status = 'OK';
    this.imageUploaded.emit(res);
    this.imageChangedEvent = null;
  }

  private onFailure(res) {
    this.croppedImage.pending = false;
    this.croppedImage.status = 'FAIL';
    this.croppedImage.src = '';
    this.imageError.emit(res);
    this.imageChangedEvent = null;
  }

  openModal(content) {
    this.modalService.open(content, { size: 'lg' });
  }

  imageCropped(file: File): FileSnippet | File {
    if (this.croppedImage) {
      return this.croppedImage.file = file;
    }

    return this.croppedImage = new FileSnippet('', file);
  }

  displayOriginalImage(input) {
    const file: File = input.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      this.originalImage = new FileSnippet(event.target.result, file)
    });

    reader.readAsDataURL(file);
  }

  processFile(imageInput: any, event: any) {
    this.displayOriginalImage(imageInput);

    this.croppedImage = undefined;

    const URL = window.URL;
    let file, img;

    if ((file = event.target.files[0]) && (file.type === 'image/png' || file.type === 'image/jpeg')) {
      img = new Image();

      const self = this;
      img.onload = function() {

        if (this.width >= FileSnippet.IMAGE_SIZE.width && this.height >= FileSnippet.IMAGE_SIZE.height) {
          self.imageChangedEvent = event;
        } else {
          self.flashMessage.show(`Minimum image size is ${FileSnippet.IMAGE_SIZE.width}x${FileSnippet.IMAGE_SIZE.height}`, {cssClass: 'alert-danger', timeout: 3000});
        }
      };

      img.src = URL.createObjectURL(file);
    } else {
      this.flashMessage.show('Unsupported file type: Only JPEG and PNG allowed.', {cssClass: 'alert-danger', timeout: 3000});
    }
  }


  uploadImage() {
    if (this.croppedImage) {
      const reader = new FileReader();

      reader.addEventListener('load', (event: any) => {
        let formData = new FormData();
        let request;

        if (this.aspectRatio === 4/3) {
          formData.append('evidence', this.originalImage.file);
          formData.append('evidence', this.croppedImage.file);

          request = {
            formData: formData,
            upload: 'multiple'
          }
        }

        else if (this.aspectRatio === 1) {
          formData.append('device', this.croppedImage.file);

          request = {
            formData: formData,
            upload: 'single'
          }
        }

        this.croppedImage.src = event.target.result;
        this.croppedImage.pending = true;
        this.uploadService.uploadImage(request).subscribe(
          (response) => {
            if (response.success) {
             this.onSuccess(response);
            } else {
             this.onFailure(response);
            }
          },
          err => {
            this.onFailure(err);
          })
      });
      reader.readAsDataURL(this.croppedImage.file);
    }
  }

}
